FROM php:7.4-alpine as builder

# Install build dependencies
RUN set -eux \
	&& apk add --no-cache \
		ca-certificates \
		curl \
		git \
	&& git clone https://github.com/squizlabs/PHP_CodeSniffer

ARG PHPCS
RUN set -eux \
	&& cd PHP_CodeSniffer \
	&& if [[ "${PHPCS}" = "latest"  ||  -z ${PHPCS} ]]; then \
		VERSION="$( git describe --abbrev=0 --tags )"; \
	else \
		VERSION="$( git tag | grep -E "^v?${PHPCS}\.[.0-9]+\$" | sort -V | tail -1 )"; \
	fi \
	&& curl -sS -L https://github.com/squizlabs/PHP_CodeSniffer/releases/download/${VERSION}/phpcs.phar -o /phpcs.phar \
	&& chmod +x /phpcs.phar \
	&& mv /phpcs.phar /usr/bin/phpcs


FROM php:7.4-alpine as production

COPY --from=builder /usr/bin/phpcs /usr/bin/phpcs
ENV WORKDIR /data
WORKDIR /data

ENTRYPOINT ["phpcs"]
CMD ["--version"]
